import 'dart:convert';

import 'package:http/http.dart';
import 'package:logger/logger.dart';

class Ethereum {
  static eth(wei) {
    return (wei / 1e13).floor() / 1e5;
  }
}

class Price {
  double USD = 0;
  double EUR = 0;
}

class Stats {
  DateTime time;
  double unpaid = 0;
  double minPayout = 0;
  double hashRate = 0;
  int workers = 0;
  int acceptedShares = 0;
  int invalidShares = 0;
  int staleShares = 0;

  Price currency = new Price();
}

class Ethermine {
  String address;
  Stats stats = new Stats();

  Logger log = Logger();

  Ethermine({this.address});

  Future<void> fetchData() async {
    if (this.address == null) {
      throw new Exception("No address defined");
    }

    final uri =
        "https://api.ethermine.org/miner/${this.address.replaceFirst('0x', '')}/dashboard";
    print(uri);
    var res = await get(Uri.parse(uri));

    if (res.statusCode != 200) {
      this.log.e("HTTP Status code: ${res.statusCode}");
      return;
    }

    var data = jsonDecode(res.body)['data'];

    this.stats.unpaid = data['currentStatistics']['unpaid'].toDouble();
    this.stats.minPayout = data['settings']['minPayout'].toDouble();
    this.stats.workers = data['workers'].length;

    // Global stats
    this.stats.acceptedShares = data['currentStatistics']['validShares'];
    this.stats.invalidShares = data['currentStatistics']['invalidShares'];
    this.stats.staleShares = data['currentStatistics']['staleShares'];
    this.stats.hashRate =
        (data['currentStatistics']['reportedHashrate'].toDouble() / 1e4)
                .floor() /
            1e2;

    this.stats.time = new DateTime.fromMillisecondsSinceEpoch(
        data['currentStatistics']['time'] * 1000);

    // Get current price
    res = await get(Uri.parse(
        'https://min-api.cryptocompare.com/data/price?fsym=ETH&tsyms=USD,EUR'));

    data = jsonDecode(res.body);

    this.stats.currency.USD = data['USD'];
    this.stats.currency.EUR = data['EUR'];
  }
}
