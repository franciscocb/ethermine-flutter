import 'package:flutter/material.dart';
import 'package:progress_hud/progress_hud.dart';

class Progress extends ProgressHUD {
  Progress()
      : super(
          backgroundColor: Colors.black12,
          color: Colors.white,
          containerColor: Colors.blue,
          borderRadius: 5.0,
          loading: false,
          text: 'Loading...',
        );

  show() {
    this.state.show();
  }

  dismiss() {
    this.state.dismiss();
  }
}
