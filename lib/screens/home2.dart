import 'package:ethm/components/Ethermine.dart';
import 'package:ethm/components/Progress.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:progress_hud/progress_hud.dart';
import 'package:qrscan/qrscan.dart' as scanner;

class EtherminePage extends StatefulWidget {
  @override
  _EtherminePageState createState() => _EtherminePageState();
}

class _EtherminePageState extends State<EtherminePage> {
  String address = '0xd683a94Fb899222c3C02E5eB7D41823717530c34';
  String __qrAddr = '0xd683a94Fb899222c3C02E5eB7D41823717530c34';
  bool _loading = false;

  double _unpaid = 0;
  double _hashrate = 0;

  Stats stats = new Stats();

  createCard(context, {title, text, expanded = true}) {
    var card = Card(
      child: Container(
        padding: EdgeInsets.all(25.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.end,
          children: <Widget>[
            Text(
              '$title',
            ),
            Text('$text', style: Theme.of(context).textTheme.headline4)
          ],
        ),
      ),
    );

    return expanded ? Expanded(child: card) : card;
  }

  fetch() async {
    setState(() {
      _loading = true;
    });

    try {
      String addr = address;
      // setState(() => {address = 'Loading data...'});
      var eth = Ethermine(address: addr);
      await eth.fetchData();
      setState(() {
        address = addr;
        stats = eth.stats;
        _loading = false;
      });
    } catch (e) {
      print(e);
    }
  }

  @override
  void initState() {
    super.initState();
    fetch();
  }

  Future<void> _showMyDialog() async {
    return showDialog<void>(
      context: context,
      barrierDismissible: false, // user must tap button!
      builder: (BuildContext context) {
        return AlertDialog(
          backgroundColor: Theme.of(context).backgroundColor,
          title: Text('Settings'),
          content: SingleChildScrollView(
            child: ListBody(
              children: <Widget>[
                Text("Wallet address:",
                    style: Theme.of(context).textTheme.subtitle2),
                Text("$__qrAddr"),
              ],
            ),
          ),
          actions: <Widget>[
            TextButton(
              child: Text('Scan QR'),
              onPressed: () async {
                String addr = await scanner.scan();

                if (addr.contains('ethereum:')) {
                  addr = addr.replaceAll("ethereum:", "");
                }

                setState(() {
                  __qrAddr = addr;
                });
              },
            ),
            TextButton(
              child: Text('Cancel'),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
            TextButton(
              child: Text('Save'),
              onPressed: () {
                setState(() {
                  address = __qrAddr;
                });

                this.fetch();
                Navigator.of(context).pop();
              },
            ),
          ],
        );
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Ethermine Page"),
        actions: [
          Padding(
              padding: EdgeInsets.only(right: 20.0),
              child: GestureDetector(
                child: Icon(Icons.more_vert),
                onTap: () {
                  _showMyDialog();
                },
              ))
        ],
      ),
      floatingActionButton: FloatingActionButton(
          onPressed: () async => {await fetch()}, child: Icon(Icons.refresh)),
      body: Stack(children: [
        Center(
          child: Column(
            children: [
              if (_loading)
                Container(
                  padding: EdgeInsets.fromLTRB(25, 0, 25, 25),
                  child: Text('Loading...'),
                ),
              Container(
                padding: EdgeInsets.fromLTRB(25, 25, 25, 25),
                child: Text("Last update: ${stats.time}",
                    style: Theme.of(context).textTheme.subtitle2),
              ),
              Row(
                children: [
                  this.createCard(
                    context,
                    title: "Unpaid balance",
                    text: "${Ethereum.eth(stats.unpaid)} ETH",
                  ),
                  this.createCard(
                    context,
                    title: "Reported Hashrate",
                    text: "${stats.hashRate} MH/s",
                  ),
                ],
              ),
              Row(
                children: [
                  this.createCard(context,
                      title: "Unpaid (USD)",
                      text:
                          "${((Ethereum.eth(stats.unpaid) * stats.currency.USD) * 100).floor() / 100} USD"),
                  this.createCard(context,
                      title: "Unpaid (EUR)",
                      text:
                          "${((Ethereum.eth(stats.unpaid) * stats.currency.EUR) * 100).floor() / 100} EUR"),
                ],
              ),
              Row(
                children: [
                  this.createCard(context,
                      title: "Min Payout",
                      text: "${Ethereum.eth(stats.minPayout)} ETH"),
                  this.createCard(context,
                      title: "Active workers", text: "${stats.workers}"),
                ],
              ),
              Row(
                children: [
                  this.createCard(context,
                      title: "Accepted Shares",
                      text: "${stats.acceptedShares}"),
                  this.createCard(context,
                      title: "Invalid Shares", text: "${stats.invalidShares}"),
                  this.createCard(context,
                      title: "Stale Shares", text: "${stats.staleShares}"),
                ],
              ),
            ],
          ),
        ),
      ]),
    );
  }
}
