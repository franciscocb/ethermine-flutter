import 'package:ethm/screens/home2.dart';
import 'package:flutter/material.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    // FlutterStatusbarcolor.setStatusBarColor(Colors.green);

    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(primarySwatch: Colors.green),
      darkTheme: ThemeData(
          // primarySwatch: Colors.teal,
          cardColor: Colors.indigo[500],
          // cardColor: Color.fromRGBO(65, 57, 56, 1),
          primarySwatch: Colors.indigo,
          scaffoldBackgroundColor: Colors.blueGrey[900],
          // scaffoldBackgroundColor: Color.fromRGBO(38, 35, 39, 1),
          backgroundColor: Color.fromRGBO(38, 35, 39, 1),
          textTheme: Typography(platform: TargetPlatform.iOS).white,
          inputDecorationTheme:
              InputDecorationTheme(labelStyle: TextStyle(color: Colors.grey))),

      // home: MyHomePage(title: 'EtherSherlock'),
      home: EtherminePage(),
    );
  }
}
